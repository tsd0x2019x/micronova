#!/bin/bash
GIT_URL=https://tsd0x2019x@bitbucket.org/tsd0x2019x/micronova.git
USER_EMAIL="tsd0x2019@googlemail.com"
USER_NAME="tsd0x2019x"

git config --global user.email $USER_EMAIL
git config --global user.name $USER_NAME

# Attempt to reset the git origin url.
git remote set-url origin "${GIT_URL}"

# If there is error, for example, because this repository is new and has no origin set yet,
# git will return the error message as "fatal: No such remote 'origin'".
# The return value for error catching is stored in variable '$?'. If the previous command 
# execution was successful, $? will be zero (0). Otherwise, it will be a number other than 0.
if [ $? -ne 0 ]; then # if variable $? is not equal 0 => NOK
	git remote add origin "${GIT_URL}" # Add a new git remote origin url
fi

# Disable password prompt
git config --global core.askPass ""

# Display all git configuration
git config --list

# Pause and wait for pressing a key to be continued
read -n1 -r -p "ENTER.." key
